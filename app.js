import express from "express";
import cookingTimeRouter from "./src/routes/cookingTime.js";
import suggestRouter from "./src/routes/suggest.js";
import morgan from "morgan";

const app = express();
app.use(express.json({ limit: "10kb" }));
if (process.env.NODE_ENV === "development") {
  app.use(morgan("dev"));
}

app.use("/", cookingTimeRouter);
app.use("/", suggestRouter);

export default app;
