# Ramadan Dishes Challenge

<p>
- This a NodeJS api using  express as framework.
- The app run on server  with  URL: `http://localhost:3000`
</p>

# Run the project

```shell
git clone https://gitlab.com/Ali_Benromdhane/ramadan_dishes.git ramadan_dishes
```

```shell
cd ramadan_dishes/
```

```shell
npm install
```

```shell
npm start
```

## Endpoint 1: Cooking time

- The first endpoint `/cooktime` accept two query params:
  - `ingredient`: The required ingredient in the dish.
  - `day`: The _Ramadan_ day in which the dish will be cooked

```js
// GET http://localhost:3000/cooktime?ingredient=Carrot&day=12
// Response:
[
  {
    name: "Shakshouka",
    ingredients: ["Carrot", "Harissa", "Black Pepper", "Garlic"],
    cooktime: "11 minutes after Asr",
  },
  {
    name: "Dweeda",
    ingredients: [
      "Dweeda",
      "Tomatoe Paste",
      "Meat",
      "Potatoe",
      "Carrot",
      "Onion",
    ],
    cooktime: "119 minutes before Asr",
  },
];
```

## Endpoint 2: Suggestions

- The second endpoint `/suggest` accept one query param:
  - `day`: The _Ramadan_ day in which the dish will be cooked
- The endpoint respond with the smallest cooking duration dish.

```js
// GET http://localhost:3000/suggest?day=15
// Response:
{
    "name": "Cannelloni",
    "ingredients": [
        "Chicken",
        "Cannelloni",
        "Onion",
        "Tomatoe Paste"
    ],
    "cooktime": "7 minutes after Asr"
}
```

## The packages used in the app

[express]: Fast, unopinionated, minimalist web framework for node.<br>
[axios]: Promise based HTTP client for the browser and node.js<br>
[dotenv] : Loads environment variables from .env file.<br>
[joi]: Validate schemas. <br>
[unirest]: Get user locations info : ip,lng,lat... <br>
[morgan]: HTTP request logger middleware for node.js. <br>
