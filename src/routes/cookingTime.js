import express from "express";
import { queryValidatorCookingTime } from "../middleware/validator.js";
import { getDishes } from "../controllers/cookingTime.js";
import { getUserLocation } from "../middleware/getUserLocation.js";

const router = express.Router();

router
  .route("/cooktime")
  .get(queryValidatorCookingTime, getUserLocation, getDishes);

export default router;
