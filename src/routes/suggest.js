import express from "express";
import { suggestDish } from "../controllers/suggest.js";
import { queryValidatorSuggest } from "../middleware/validator.js";
import { getUserLocation } from "../middleware/getUserLocation.js";
const router = express.Router();

router
  .route("/suggest")
  .get(queryValidatorSuggest, getUserLocation, suggestDish);

export default router;
