import unirest from "unirest";
import { asyncMiddleware } from "../utils/catchAsync.js";

export const getUserLocation = asyncMiddleware(async (req, res, next) => {
  var apiCall = unirest(
    "GET",
    "https://ip-geolocation-ipwhois-io.p.rapidapi.com/json/"
  );
  apiCall.headers({
    "x-rapidapi-host": "ip-geolocation-ipwhois-io.p.rapidapi.com",
    "x-rapidapi-key": "srclZqaa9imshAk9Xzz55u27oltLp1SqdiFjsnmva9PTpf2j3f",
  });
  apiCall.end(function (result) {
    if (res.error) throw new Error(result.error);
    req.userLocation = {
      latitude: result.body.latitude,
      longitude: result.body.longitude,
    };
    next();
  });
});
