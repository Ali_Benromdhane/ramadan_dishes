import Joi from "joi";
import { createValidator } from "express-joi-validation";
const validator = createValidator({});

const querySchemaCookingTime = Joi.object({
  day: Joi.number().integer().min(1).max(30).required(),
  ingredient: Joi.string().min(2).max(15).required(),
});

const querySchemaSuggest = Joi.object({
  day: Joi.number().integer().min(1).max(30).required(),
});

export const queryValidatorCookingTime = validator.query(
  querySchemaCookingTime
);
export const queryValidatorSuggest = validator.query(querySchemaSuggest);
