import { getPrayers } from "../api/prayers.js";

export const capitalizeFirstLetter = (str) => {
  return str.charAt(0).toUpperCase() + str.slice(1);
};

const minTwoDigits = (day) => {
  return (day < 10 ? "0" : "") + day;
};

export const durationBetweenAsrAndMaghreb = (maghrebTime, asrTime) => {
  return maghrebTime - asrTime - 15;
};

export const convertHourstoMinute = (str) => {
  const time = str.split(" ");
  const [hours, minutes] = time[0].split(":");

  return +hours * 60 + +minutes;
};

export const calculateDifferenceTime = (todayPrayers) =>
  durationBetweenAsrAndMaghreb(
    convertHourstoMinute(todayPrayers.Maghrib),
    convertHourstoMinute(todayPrayers.Asr)
  );

export const printCookingtimeComparedToAsrTime = (difference, arr) =>
  arr.map((dish) => {
    return difference > dish.duration
      ? (dish.cooktime = `${difference - dish.duration} minutes after Asr`)
      : (dish.cooktime = `${dish.duration - difference} minutes before Asr`);
  });

export const getPrayersTimeWithGivenDay = async (day, lat, lng) => {
  const prayers = await getPrayers(lat, lng);
  const filtered = prayers.data.filter(
    (el) => minTwoDigits(day) === el.date.hijri.day
  );
  return filtered[0].timings;
};

export const getSpecificFieldsFromDish = (arr) => {
  return arr.map((dish) =>
    (({ name, ingredients, cooktime }) => ({ name, ingredients, cooktime }))(
      dish
    )
  );
};

export const getHijriDate = (dayInMS = Date.now()) => {
  const date = new Intl.DateTimeFormat("en-TN-u-ca-islamic", {
    month: "numeric",
    year: "numeric",
  }).format(dayInMS);
  const month = date.split("/")[0];
  const year = date.split("/")[1];
  return { month, year };
};
