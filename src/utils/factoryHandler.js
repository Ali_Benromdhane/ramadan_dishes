import { asyncMiddleware } from "./catchAsync.js";
import { dishesList } from "../data/dishes.js";
import {
  capitalizeFirstLetter,
  printCookingtimeComparedToAsrTime,
  getPrayersTimeWithGivenDay,
  getSpecificFieldsFromDish,
  calculateDifferenceTime,
} from "./helperMethods.js";

export const dishHandler = () =>
  asyncMiddleware(async (req, res, next) => {
    const { latitude, longitude } = req.userLocation;
    const { day, ingredient } = req.query;
    const capitalizeIngredient = ingredient
      ? capitalizeFirstLetter(ingredient)
      : "";
    const dishes = ingredient
      ? dishesList.filter((dish) =>
          dish.ingredients.includes(capitalizeIngredient)
        )
      : dishesList;
    const prayersTime = await getPrayersTimeWithGivenDay(
      day,
      latitude,
      longitude
    );
    const diff = calculateDifferenceTime(prayersTime);
    printCookingtimeComparedToAsrTime(diff, dishes);

    const filteredDishes = getSpecificFieldsFromDish(dishes);
    const minDishDuration = Math.max(
      ...filteredDishes.map((d) => d.cooktime.split(" ")[0])
    );
    const result = ingredient
      ? filteredDishes
      : filteredDishes.find(
          (d) => d.cooktime.split(" ")[0] === `${minDishDuration}`
        );
    res.json({ dishes: result });
  });
