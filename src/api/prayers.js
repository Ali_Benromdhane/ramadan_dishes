import axios from "axios";
import { getHijriDate } from "../utils/helperMethods.js";

const url = "http://api.aladhan.com/v1/hijriCalendar";

export const fetchPrayersTime = (lat, lng) => {
  const { month, year } = getHijriDate();
  const newYear = parseInt(month) > 9 ? parseInt(year) + 1 : year;
  const ramadanMonth = 9;

  return axios.get(
    `${url}?latitude=${lat}&longitude=${lng}&method=1&month=${ramadanMonth}&year=${newYear}`
  );
};

export const getPrayers = async (lat, lng) => {
  try {
    const { data } = await fetchPrayersTime(lat, lng);
    return data;
  } catch (error) {
    console.log(error.message);
  }
};
